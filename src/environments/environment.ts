// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  marvelUrl: 'http://gateway.marvel.com/v1/public/',
  marvelApikey : '22658069b36d5abde79ed5d15e0ab163',
  marvelHash: '33bb88d7dfbd882f0012a878c33f3436',
  marvelTs: 1000,
  firebase : {
    apiKey: "AIzaSyBVniW1Wv04-NMLxDOqCLNGXnu7LTCytz8",
    authDomain: "mvp-cfc48.firebaseapp.com",
    projectId: "mvp-cfc48",
    storageBucket: "mvp-cfc48.appspot.com",
    messagingSenderId: "248580569013",
    appId: "1:248580569013:web:d8805d7df934bb4c4d0bfc",
    measurementId: "G-GLY66X3V53",
    databaseURL: 'https://mvp-cfc48.firebaseio.com'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
