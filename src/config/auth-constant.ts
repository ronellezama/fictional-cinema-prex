export class AuthConstants {
    public static readonly AUTH = 'userDataMVP'
    public static readonly VERSION = {
        "android": '0.0.3',
        "ios": '0.0.1'
    }
};