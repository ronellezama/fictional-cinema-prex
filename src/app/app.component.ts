import { Component, Input  } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthService} from './services/auth.service'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  name : string = "";
  imageProfile : string = "";
  user : any;
  session = true;

  constructor(private afAuth:AngularFireAuth,private router:Router,
    private menu:MenuController,private authService:AuthService) {
      this.session = this.authService.getCurrentUser() ? true : false;
  }
  ngOnInit() {
    this.session = this.authService.getCurrentUser() ? true : false;
    setInterval(()=>{
      this.session = this.authService.getCurrentUser() ? true : false;
    },100);
 


  }
  logout(){
    return this.afAuth.signOut().then(() => {
     this.authService.logout();
     this.session =  false;
      this.router.navigate(['login']);
    })
  }
 
}
