import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular'; 
import { ActivatedRoute,Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showPassword:boolean =  false; 
  postData={
    email:"",
    password:""
  }
  loading: any;
  constructor(
    public toastController: ToastController,
    private router:Router, 
    private loadingController:LoadingController,
    private afAuth: AngularFireAuth,
    private authService:AuthService) { }

  ngOnInit() {
    let session = this.authService.getCurrentUser() ? true : false;
    if(session){
      this.router.navigate(['/home']);
    }
  }
  async presentToast(message, type) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: type
    });
    toast.present();
  }

  toggleShow() {
    this.showPassword = !this.showPassword ? true : false;
  }

  async login(){
    this.presentLoading();
    if (this.validateInputs()) {
      await this.afAuth.signInWithEmailAndPassword(this.postData.email, this.postData.password)
        .then(result => {
          var userAuth = firebase.default.auth().currentUser;
          console.log("usuario", userAuth);
          this.dismissLoading(this.loading);
          this.presentToast("Ingreso satisfactorio",'success');          
          this.authService.setCurrentUser(this.postData.email);//Session var
          this.router.navigate(['/home'])
        }).catch(err => {
          console.log(`login failed ${err}`);
          this.presentToast(err.message,'danger');
          this.dismissLoading(this.loading);
        });
    } else {
      this.presentToast("Por favor ingrese su email y contraseña",'danger');
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Procesando información ...',
      duration: 500000
    });
    this.loading.present();
  }

  async dismissLoading(loading) {
    await this.loading.dismiss();
    this.loading = await this.loadingController.create({
      message: 'Procesando información ...',
      duration: 500000
    });
  }

  validateInputs() {
    let email = this.postData.email.trim();
    let password = this.postData.password.trim();
    return (
      this.postData.email &&
      this.postData.password &&
      email.length > 0 &&
      password.length > 0
    );
  }



}
