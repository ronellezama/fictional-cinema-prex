import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular'; 
import { ActivatedRoute,Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
@Component({
  selector: 'app-recovery-password',
  templateUrl: './recovery-password.page.html',
  styleUrls: ['./recovery-password.page.scss'],
})
export class RecoveryPasswordPage implements OnInit {
  email:string ="";
  success:boolean = false;
  constructor(
    public toastController: ToastController,
    private router:Router, 
    private loadingController:LoadingController,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  goBack(){
    this.router.navigate(['/login']);
  }

  recovery() {
    if(this.email.length == 0){
      this.presentToast('Emails is required','danger');
    }
    var auth = firebase.default.auth();
    return auth.sendPasswordResetEmail(this.email)
      .then(() =>{
        console.log("email send")
        this.success=true;
        this.presentToast('Emails is required','danger');
      })
      .catch((error) => {
        console.log(error)
        this.presentToast(error.message,'danger');
      })
   
  }

  async presentToast(message, type) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: type
    });
    toast.present();
  }



}
