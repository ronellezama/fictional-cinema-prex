import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiMarvelService {

  constructor(public http: HttpClient) { }

  getComics(offset){     
    let endPointComicsString = environment.marvelUrl+'comics';    
    let paramsComics = '?ts='+environment.marvelTs+'&apikey='+environment.marvelApikey+'&hash='+environment.marvelHash+'&offset='+offset
    return this.http.get(endPointComicsString+paramsComics);
  }

  getComic(id){     
    let endPointComicsString = environment.marvelUrl+'comics/'+id;    
    let paramsComics = '?ts='+environment.marvelTs+'&apikey='+environment.marvelApikey+'&hash='+environment.marvelHash
    return this.http.get(endPointComicsString+paramsComics);
  }
}
