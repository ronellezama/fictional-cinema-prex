import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  // Set current user in your session after a successful login
  setCurrentUser(email: string) {
    sessionStorage.setItem('user', email);
    return true;
  }

  // Get currently logged in user from session
  getCurrentUser(): string | any {
    return sessionStorage.getItem('user') || undefined;
  }

  // Clear the session for current user & log the user out
  logout() {
    sessionStorage.removeItem('user');
    return false;
    // ... other code for logout
  }
}
