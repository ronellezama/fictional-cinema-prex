import { Component, NgZone } from '@angular/core';
import {ApiMarvelService } from '../services/api-marvel.service';
import { Router, NavigationExtras } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  allData:any
  data = []
  infiniteScroll : any;
  offset = 0;
  lentghData = 0;
  constructor(private apiMarvelService:ApiMarvelService, private _zone: NgZone, private router:Router) {}

  ngOnInit() {
    this.getComics();
  }

  getComics(){
    this.apiMarvelService.getComics(this.offset).subscribe(
      (res:any)=>{
     
       this._zone.run(() =>{
        this.allData = res.data
        for(let i=0; i < res.data.results.length ; i++){    
          this.data.push(res.data.results[i]);
        }
        this.lentghData = this.data.length;
       });
       
       if(this.infiniteScroll)
        this.infiniteScroll.target.complete();

      },(fail:any)=>{
        console.log("fail",fail)
      }
    );
  }

  doInfinite(infiniteScroll) {
    this.infiniteScroll = infiniteScroll;
    this.offset = this.offset + 21;
    this.getComics();
  }

  goDetail(id){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify({id:id})
      }, 
      //skipLocationChange: true
    };
    this.router.navigate(['/detail-movie'], navigationExtras);
  }

}
