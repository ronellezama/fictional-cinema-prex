import { Component, OnInit } from '@angular/core';
import {ApiMarvelService } from '../services/api-marvel.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-detail-movie',
  templateUrl: './detail-movie.page.html',
  styleUrls: ['./detail-movie.page.scss'],
})
export class DetailMoviePage implements OnInit {
  id = null;
  movie = {
    id:null,
    title:null,
    image:null,
    extension:null,
    description:null,
    date:null,
    rate:0
  }
  constructor(private apiMarvelService:ApiMarvelService,private router:Router, private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
       // console.log( JSON.parse(params.special))
        let data = JSON.parse(params.special);
        this.movie.id = data.id;
      }
    });
  }

  ngOnInit() {
    this.getComic()
  }

  getComic(){
    this.apiMarvelService.getComic(this.movie.id).subscribe(
      (res:any)=>{
     
      if(res.data.results.length > 0){
        console.log("res",res.data.results[0])
        this.movie.title = res.data.results[0].title;
        this.movie.image = res.data.results[0].images.length > 0 ? res.data.results[0].images[0].path : res.data.results[0].thumbnail.path ;
        this.movie.extension = res.data.results[0].images.length > 0 ? res.data.results[0].images[0].extension : res.data.results[0].thumbnail.extension ;
        this.movie.description = res.data.results[0].description ? res.data.results[0].description : 'Sin detalles';
        this.movie.date = this.formatDate(res.data.results[0].dates[1].date);
        console.log("movie",this.movie)
      }

      },(fail:any)=>{
        console.log("fail",fail)
      }
    );
  }

  
  formatDate(date) {
    var res = date.split("T");
    var dt = res[0].split("-");
    return dt[2] + "/" + dt[1] + "/" + dt[0];

  }


}
