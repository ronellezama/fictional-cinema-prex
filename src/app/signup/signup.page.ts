import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  showPassword: boolean = false;
  showPasswordConfirm: boolean = false;
  password: string = ""
  passwordConfirm: string = ""
  postData = {
    email: "",
    password: ""
  }
  loading: any;
  differents = false;
  constructor(
    public toastController: ToastController,
    private loadingController: LoadingController,
    private afAuth: AngularFireAuth,
    private authService:AuthService,
    private router:Router) { }

  ngOnInit() {
    // this.presentToast()
    console.log(this.showPassword)

  }

  async presentToast(message, type) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: type
    });
    toast.present();
  }


  toggleShow(confirm) {
    if (!confirm)
      this.showPassword = !this.showPassword ? true : false;
    else
      this.showPasswordConfirm = !this.showPasswordConfirm ? true : false;
    // this.passwordInput.type = this.showPassword ? 'text' : 'password';
  }

  signUp() {
    if(this.postData.password !== this.passwordConfirm) {
      this.presentToast("Las contraseñas no coinciden",'danger');
      return false;
    }
    this.afAuth
      .createUserWithEmailAndPassword(this.postData.email, this.postData.password)
      .then(res => {
         console.log("trae",firebase.default.auth().currentUser);
        
         this.afAuth.signInWithEmailAndPassword(this.postData.email, this.postData.password)
        .then(result => {
          var userAuth = firebase.default.auth().currentUser;
          console.log("usuario", userAuth);
          this.dismissLoading(this.loading);
          this.presentToast("Registro satisfactorio",'success');
          this.authService.setCurrentUser(this.postData.email);//Session var
          this.router.navigate(['/home'])
        }).catch(err => {
          console.log(`login failed ${err}`);
          this.presentToast(err.message,'danger');
          this.dismissLoading(this.loading);
        });
      
      })
      .catch(err => {
        this.dismissLoading(this.loading);
        console.log(`login failed ${err}`);
        this.presentToast(err.message,'danger');
        //this.error = err.message;
      });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Procesando información ...',
      duration: 500000
    });
    this.loading.present();
  }

  async dismissLoading(loading) {
    await this.loading.dismiss();
    this.loading = await this.loadingController.create({
      message: 'Procesando información ...',
      duration: 500000
    });
  }

  validateInputs() {
    let email = this.postData.email.trim();
    let password = this.postData.password.trim();
    return (
      this.postData.email &&
      this.postData.password &&
      email.length > 0 &&
      password.length > 0
    );
  }
  checkPasswords(event){
    
    if(this.postData.password !== event.target.value) {
      this.differents = true;
    }else{
      this.differents = false;
    }
  }

}
